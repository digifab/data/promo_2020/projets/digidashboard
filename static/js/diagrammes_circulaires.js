(async()=>{

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
const testfetch = await axios.get('/datadiagrammes_circulaires');

const dataTest = testfetch.data;

// Console.log permet de voir dans l'inspecteur de la page web si la donnée est
// correctement récupérée.
console.log(testfetch)

for(let i = 0; i < dataTest.length; i++){

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
  let newDiv = document.createElement('div');
  newDiv.id = dataTest[i].table;
  document.body.appendChild(newDiv);

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create(dataTest[i].table, am4charts.PieChart3D);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = dataTest[i].data;

chart.innerRadius = am4core.percent(40);
chart.depth = 120;

chart.legend = new am4charts.Legend();

var series = chart.series.push(new am4charts.PieSeries3D());
series.dataFields.value = "litres";
series.dataFields.depthValue = "litres";
series.dataFields.category = "country";
series.slices.template.cornerRadius = 5;
series.colors.step = 40;
}
})()