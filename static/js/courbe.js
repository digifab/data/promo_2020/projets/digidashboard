(async()=>{

// "await" permet au JS de terminer de charger la route avant de passer à la 
// ligne suivante.
// axios.get permet de recupérer une donnée.
const testfetch = await axios.get('/datacourbe');

const dataTest = testfetch.data;

console.log(testfetch.data)
// Boucle qui itère sur les données du Json
for(let i = 0; i < dataTest.length; i++){

// Créer les div sur le html, creer l'id en fonction de l'indice i, puis les
// places l'une en dessous de l'autre.
  let newDiv = document.createElement('div');
  newDiv.id = dataTest[i].table;
  document.body.appendChild(newDiv);

// Create chart instance
var chart = am4core.create(dataTest[i].table, am4charts.XYChart);

am4core.useTheme(am4themes_animated);

// Add data
chart.data = dataTest[i].data;

// Create axes
var valuesAxis = chart.xAxes.push(new am4charts.ValueAxis());
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
var series = chart.series.push(new am4charts.LineSeries());
series.dataFields.valueY = "y";
series.dataFields.valueX = "x";
series.tooltipText = "{value}"
series.strokeWidth = 2;
series.minBulletDistance = 10;

// Make bullets grow on hover
var bullet = series.bullets.push(new am4charts.CircleBullet());
bullet.circle.strokeWidth = 2;
bullet.circle.radius = 4;
bullet.circle.fill = am4core.color("#fff");

chart.scrollbarX = new am4core.Scrollbar();

var bullethover = bullet.states.create("hover");
bullethover.properties.scale = 1.3;

chart.cursor = new am4charts.XYCursor();
chart.cursor.fullWidthLineX = true;
chart.cursor.lineX.fill = am4core.color("#000");
chart.cursor.lineX.fillOpacity = 0.1;

valuesAxis.title.text = "Les x";
valueAxis.title.text = "Les y";
}
})()