(async()=>{

const testfetch = await axios.get('/datascatter');

const dataTest = testfetch.data;


for(let i = 0; i < dataTest.length; i++){

  let newDiv = document.createElement('div');
  newDiv.id = dataTest[i].table;
  document.body.appendChild(newDiv);

// Create chart instance
var chart = am4core.create(dataTest[i].table, am4charts.XYChart);

am4core.useTheme(am4themes_animated);

// Add data
chart.data = dataTest[i].data;

// Create axes
var valueAxisX = chart.xAxes.push(new am4charts.ValueAxis());
valueAxisX.title.text = 'X Axis';
valueAxisX.renderer.minGridDistance = 40;

// Create value axis
var valueAxisY = chart.yAxes.push(new am4charts.ValueAxis());
valueAxisY.title.text = 'Y Axis';

// Create series
var lineSeries = chart.series.push(new am4charts.LineSeries());
lineSeries.dataFields.valueY = "ay";
lineSeries.dataFields.valueX = "ax";
lineSeries.strokeOpacity = 0;

var lineSeries2 = chart.series.push(new am4charts.LineSeries());
lineSeries2.dataFields.valueY = "by";
lineSeries2.dataFields.valueX = "bx";
lineSeries2.strokeOpacity = 0;

// Add a bullet
var bullet = lineSeries.bullets.push(new am4charts.Bullet());

// Add a triangle to act as am arrow
var arrow = bullet.createChild(am4core.Circle);
arrow.horizontalCenter = "middle";
arrow.verticalCenter = "middle";
arrow.strokeWidth = 0;
arrow.fill = chart.colors.getIndex(0);
arrow.direction = "top";
arrow.width = 12;
arrow.height = 12;

// Add a bullet
var bullet2 = lineSeries2.bullets.push(new am4charts.Bullet());

// Add a triangle to act as am arrow
var arrow2 = bullet2.createChild(am4core.Circle);
arrow2.horizontalCenter = "middle";
arrow2.verticalCenter = "middle";
arrow2.rotation = 180;
arrow2.strokeWidth = 0;
arrow2.fill = chart.colors.getIndex(3);
arrow2.direction = "top";
arrow2.width = 12;
arrow2.height = 12;

//add the trendlines
var trend = chart.series.push(new am4charts.LineSeries());
trend.dataFields.valueY = "value2";
trend.dataFields.valueX = "value";
trend.strokeWidth = 2
trend.stroke = chart.colors.getIndex(0);
trend.strokeOpacity = 0.7;
trend.data = [
  { "value": 1, "value2": 2 },
  { "value": 12, "value2": 11 }
];

var trend2 = chart.series.push(new am4charts.LineSeries());
trend2.dataFields.valueY = "value2";
trend2.dataFields.valueX = "value";
trend2.strokeWidth = 2
trend2.stroke = chart.colors.getIndex(3);
trend2.strokeOpacity = 0.7;
trend2.data = [
  { "value": 1, "value2": 1 },
  { "value": 12, "value2": 19 }
];

//scrollbars
chart.scrollbarX = new am4core.Scrollbar();
chart.scrollbarY = new am4core.Scrollbar();
}
})()