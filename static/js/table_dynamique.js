/**
 * Create the chart
 */
function createChart(data) {
  
  return AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "dataProvider": data,
    "valueAxes": [{
      "gridAlpha": 0.07,
      "position": "left",
      "title": "Traffic incidents"
    }],
    "graphs": [{
      "lineThickness": 2,
      "bullet": "round",
      "title": "Cars",
      "valueField": "cars"
    }, {
      "lineThickness": 2,
      "bullet": "diamond",
      "bulletSize": 12,
      "title": "Motorcycles",
      "valueField": "motorcycles"
    }, {
      "lineThickness": 2,
      "bullet": "square",
      "title": "Bicycles",
      "valueField": "bicycles"
    }],
    "chartCursor": {},
    "categoryField": "year",
    "categoryAxis": {
      "startOnAxis": true
    }
  });
  
}


console.log(jsondata)

/**
 * Define initial data
 */
var sourceData = [
  [ 1994, 1587, 650, 121 ],
  [ 1995, 1567, 683, 146 ],
  [ 1996, 1617, 691, 138 ],
  [ 1997, 1630, 642, 127 ],
  [ 1998, 1660, 699, 105 ],
  [ 1999, 1683, 721, 109 ],
  [ 2000, 1691, 737, 112 ],
  [ 2001, 1298, 680, 101 ],
  [ 2002, 1275, 664, 97 ],
  [ 2003, 1246, 648, 93 ],
  [ 2004, 1318, 697, 111 ],
  [ 2005, 1213, 633, 87 ],
  [ 2006, 1199, 621, 79 ],
  [ 2007, 1110, 210, 81 ],
  [ 2008, 1165, 232, 75 ],
  [ 2009, 1145, 219, 88 ],
  [ 2010, 1163, 201, 82 ],
  [ 2011, 1180, 285, 87 ],
  [ 2012, 1159, 277, 71 ]
];

/**
 * Create the Handsontable editable grid
 */
AmCharts.ready(function() {
  
  var container = document.getElementById("data");
  var chart;
  
  var hot = new Handsontable(container, {
    "data": sourceData,
    "height": 234,
    "colHeaders": true,
    "rowHeaders": true,
    "stretchH": 'all',
    "columnSorting": true,
    "contextMenu": true,
    "afterInit": function(firstTime) {
      chart = createChart(
        formatChartData( this.getData() )
      );
    },
    "afterChange": function(changes, source) {
      if (changes === null)
        return;
      chart.dataProvider = formatChartData(this.getData());
      chart.validateData();
    }
    
  });
  
  // define function which reformats table data into
  // format suitable for the chart
  function formatChartData(tableData) {
    var chartData = [];
    for(var i = 0; i < tableData.length; i++) {
      chartData.push({
        "year":         tableData[i][0],
        "cars":         tableData[i][1],
        "motorcycles":  tableData[i][2],
        "bicycles":     tableData[i][3]
      });
    }
    return chartData;
  }
 
});
