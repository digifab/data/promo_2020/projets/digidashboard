#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 12:46:08 2020

@author: dev

Module de graphiques et d'analyses pour les données fictives créées.
Les fonctions statistiques permettent de voir rapidement les caractéristiques
d'un set de données. 
Les fonctions graphiques proposent une visualisation des écarts types/moyennes
ainsi que de possibles corrélations.

TODO :
    - rajouter le graph clustermap // fait
    - fusionner fonction violinplot et lineplot
    - ajouter mode trimestriel (compa date avec date_range 3M ?)
    - ajout nom de fichier en param // fais
    - pouvoir sélectionner 2 colonnes à mettre en corrélation pour graph_corr
"""

import seaborn as sns
import matplotlib.pyplot as plt
import cvxpy as cp
import datetime as date
import pandas as pd
import numpy as np
import matplotlib.patches as mpatches

from auspot_donnees_fictives import generation_produits as genp, \
generation_ventes as genv, generation_data_corr as gencorr, \
generation_ventes_opti as genopt

### STATS
def stats_general(ventes):
    mean_achats = ventes["prix_achat"].mean()
    print("prix moyen des achats fournisseur : ", mean_achats)
    std_achats = ventes["prix_achat"].std()
    print("écart type des achats fournisseur : ", std_achats)
    mean_ventes = ventes["prix_vente"].mean()
    print("prix moyen des ventes client : ", mean_ventes)
    std_ventes = ventes["prix_vente"].std()
    print("écart type des ventes : ", std_ventes)


# pour un flotteur donné
def stats_produit(id_prod, ventes):
    flotteur = ventes[ventes["id_produit"] == id_prod]
    print("POUR LE FLOTTEUR D'ID :", id_prod)
    flotteur_vente_std = flotteur["prix_vente"].std()
    print("écart type vente :", flotteur_vente_std)
    flotteur_reduc_mean = flotteur["réduction"].mean()
    print("moyenne des réducs :", flotteur_reduc_mean)
    flotteur_marge_std = flotteur["marge"].std()
    print("écart type marge :", flotteur_marge_std)
    flotteur_vente_mean = flotteur["prix_vente"].mean()
    print("prix moyen des ventes :", flotteur_vente_mean)
    

    
# def repartition_stocks(w, date_debut, date_fin):
#     ventes_produits = genopt(date_debut, date_fin)
#     produits = set(ventes_produits['produit'])
#     df = pd.DataFrame([produits, w.value]).T
#     df.columns = ["produits", "pourcentage du stock"]
#     df['pourcentage du stock'] = df['pourcentage du stock'].apply(lambda x: x*100)
    
#     return df
 
    
### GRAPHS
def violinplot_analyse(donnees, mode, cible, fichier):
    f, ax = plt.subplots(figsize=(12, 6))
    
    sns.violinplot(x=mode, y=cible, data = donnees, 
                   inner = None)
    sns.swarmplot(x=mode, y=cible, data = donnees, 
              color = "white", edgecolor = "gray")
    
    plt.title(cible, color='#3266cd', size=30)
    
    plt.savefig("static/" +fichier)
    plt.show()
    
    
def lineplot_analyse(donnees, mode, cible, fichier):
    lineplot = sns.lineplot(x=mode, y=cible, data=donnees)
    lineplot.grid(color='lightgrey', linestyle='-')
    
    plt.title(cible, color='#3266cd', size=15)
    leg_std = mpatches.Patch(color='#c2d1f0',
                             label='67% de chance d\'être dans l\'interval')
    leg_mean = mpatches.Patch(color='#3266cd', label='moyenne')
 
    plt.legend(handles=[leg_std, leg_mean],
               loc='lower left',
               bbox_to_anchor=(0., -0.33))
    
    plt.savefig("static/"+fichier)
    plt.show()
    

def graph_corr(donnees, fichier):
    """
    Possibilité de filtrer la df avant de faire le .corr() pour choisir 
    quelles colonnes inspecter
    """
    graphique = sns.clustermap(donnees.corr())
    
    graphique.fig.suptitle(
        'Correlation des Facteurs',
        size=40)
    plt.tight_layout()
    
    # plt.title("heatmap de correlation", color='DarkOrchid', size=20)
    
    plt.savefig("static/" +fichier)
    plt.show()

def graph_frontiere_efficiente(date_debut, date_fin, donnees_ventes, fichier):
    """
    D'après l'exemple de CVXPY : https://bit.ly/3er67ru
    """
    mask = ((donnees_ventes["date_achat"] >= date_debut)
                    & (donnees_ventes["date_achat"] <= date_fin))
    donnees_ventes = donnees_ventes[mask]
    # récupération et préparation des données
    produits = set(donnees_ventes['produit'])
    donnees = pd.DataFrame(columns=produits, index=donnees_ventes.index)
    for produit in produits:
        donnees[produit] = donnees_ventes[donnees_ventes['produit'] 
                                      == produit]['prix_vente']
    n = len(set(donnees_ventes['produit'])) 
    mu = np.array(donnees.mean().fillna(0))
    Sigma = np.array(donnees.cov().fillna(0))
        
    # tracé graph
    w = cp.Variable(n)
    gamma = cp.Parameter(nonneg=True)
    ret = mu.T*w 
    risk = cp.quad_form(w, Sigma)
    prob = cp.Problem(cp.Maximize(ret - gamma*risk), 
                   [cp.sum(w) == 1, 
                    w >= 0])
    
    SAMPLES = 100
    risk_data = np.zeros(SAMPLES)
    ret_data = np.zeros(SAMPLES)
    gamma_vals = np.logspace(-2, 3, num=SAMPLES)
    for i in range(SAMPLES):
        gamma.value = gamma_vals[i]
        prob.solve()
        risk_data[i] = cp.sqrt(risk).value
        ret_data[i] = ret.value
        
    # markers_on = [29, 40]
    markers_on = [29]
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(risk_data, ret_data, 'g-')
    for marker in markers_on:
        plt.plot(risk_data[marker], ret_data[marker], 'bs')
        ax.annotate(r"$\gamma = %.2f$" % gamma_vals[marker], 
                    xy=(risk_data[marker]+.08, ret_data[marker]-.03))
    for i in range(n):
        plt.plot(cp.sqrt(Sigma[i,i]).value, mu[i], 'ro')
        
    plt.title("La Frontière Efficiente en fonction de la gestion ses stocks",
              color='ForestGreen', size=14)
    frontiere = mpatches.Patch(color='ForestGreen',
                             label='courbe où rapport CA/risque est optimal')
    p_inflection = mpatches.Patch(
        color='blue',
        label='point d\'inflection, avec le meilleur rapport CA/risque')
    p_possibilites = mpatches.Patch(
        color='red',
        label='points non efficients avec stocks répartis aléatoirement')
 
    plt.legend(handles=[frontiere, p_inflection, p_possibilites],
               loc='lower left',
               bbox_to_anchor=(0., -0.33))        
        
    plt.xlabel('Risque (écart type des profits)')
    plt.ylabel("Chiffre d'affaire espéré (moyenne)")
    plt.savefig("static/" +fichier)
    plt.show()
  
    return w, produits, donnees_ventes
    
    
def graph_repartition_stocks(w, produits, donnees_ventes, montant, fichier):
    df = pd.DataFrame([produits, w.value]).T
    df.columns = ["produits", "pourcentage du stock"]
    plt.clf()
    graph = sns.barplot(x="produits", y="pourcentage du stock", data=df)
    graph.tick_params(axis='x', labelrotation=70)
    plt.tight_layout()
    plt.savefig("static/" +fichier)
    plt.show()
    
    df['pourcentage du stock'] = df['pourcentage du stock'].apply(lambda x: x*100)
    
    categories = {}
    prix_achats = {}
    for x in produits:
        categories[x] = donnees_ventes.loc[donnees_ventes['produit'] 
                                           == x, 'catégorie'].unique()[0]
        prix_achats[x] = donnees_ventes.loc[donnees_ventes['produit'] 
                                            == x, 'prix_achat'].unique()[0]
    df['catégorie'] = categories.values()
    df['prix_unitaire'] = prix_achats.values()
    df['pourcentage du stock'] = df['pourcentage du stock'].round(2)
    df['prix total'] = df['pourcentage du stock'].apply(lambda x: (x/100)*montant).round(0)
    for prix in df['prix_unitaire']:
        df['quantité'] = df['prix total'].apply(lambda x: x/prix).round(0)

    return df


### TESTS
if __name__ == '__main__':
    produits_fictifs = genp()
    frequence = "d"
    date_debut = date.datetime(2018, 1, 1)
    date_fin = date.datetime(2020, 12, 31)
    ventes = genv(produits_fictifs, date_debut, date_fin, frequence)
    
    
    ### TESTS STATS
    stats_gen = False
    stats_prod = False
    opti_stocks = False
    ### TESTS GRAPHS
    analyse_violinplot = False
    analyse_lineplot = False
    analyse_correlation = True
    frontiere_efficiente = False
    # lancer frontiere efficiente en même temps que opti stock
    opti_stocks_graph = False
    
    if stats_gen:
       stats_general(ventes)
       
    if stats_prod:
        id_prod = 1
        stats_produit(id_prod, ventes)
    
    # if opti_stocks:
    #     date_debut = date.datetime(2018, 1, 1)
    #     date_fin = date.datetime(2020, 12, 31)
    #     fichier = "images/frontiere_efficiente.png"
    #     w = graph_frontiere_efficiente(date_debut, date_fin, fichier)
    #     repartition_stocks(w, date_debut, date_fin)
        
    if analyse_violinplot:
        mode = "annee"
        # nom de colonne à cibler dans la df de données
        cible = "marge"
        fichier = "images/violinplot.png"
        violinplot_analyse(ventes, mode, cible, fichier)
        
    if analyse_lineplot:
        mode = "mois"
        cible = "prix_vente"
        fichier = "images/lineplot.png"
        lineplot_analyse(ventes, mode, cible, fichier)
    
    if analyse_correlation:
        fichier = "images/graph_correlation.png"
        date_debut = date.datetime(2020, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        donnees = gencorr(date_debut, date_fin)
        donnees_corr = donnees[["combi été", "combi hiver", "protec sol"]]
        graph_corr(donnees_corr, fichier)
    
    if frontiere_efficiente:
        date_debut = date.datetime(2020, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        fichier = "images/frontiere_efficiente.png"
        donnees_ventes = genopt(date_debut, date_fin)
        graph_frontiere_efficiente(date_debut, date_fin, donnees_ventes, 
                                   fichier)
        
    if opti_stocks_graph:
        fichier = "images/frontiere_efficiente.png"
        date_debut = date.datetime(2018, 1, 1)
        date_fin = date.datetime(2020, 12, 31)
        donnees_ventes = genopt(date_debut, date_fin)
        w, produits, donnees_ventes = graph_frontiere_efficiente(date_debut, 
                                                                 date_fin, 
                                                                 donnees_ventes, 
                                                                 fichier)
        fichier = "images/repartition.png"
        graph_repartition_stocks(w, produits, donnees_ventes, fichier)