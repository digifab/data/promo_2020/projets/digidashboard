# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

La page utilisateur, il n'a qu'appeler les classes pour créer son dashboard.

Ne pas oublier d'importer DigiDashBoard.

TODO : Utiliser Vu.js pour avoir un mecanisme de tableau modifiable (input),
si des graphes sont affichés, le résultat change en conséquence.

(modifier les données d'un graph avec un tableau (mathplotlib))

@author: Nicolas, Reda
"""

import DigiDashBoard as DDB
import Flaskdigidashboard as fdb

graphiques = {
            "graphique1": "Histogramme",
            "graphique2": "Scatter",
            "graphique3": "Courbe",
            "graphique4": "Diagrammes_circulaires",
            "graphique5": "Table"
            }

boutonstitres = {
                "namedb1": "Digi",
                "namedb2": "DashBoard",
                "bouton1": "Dashboards",
                "soustitre1": "Graphique",
                "soustitre2": "Gestion stock",
                "soustitre3": "Commerce",
                "titrehisto": "Les historgrammes",
                "textehisto": "Cette page regroupe les histogrammes du Digidashboard"
                }



boutonstitres = DDB.Boutonstitres(boutonstitres)
graphiques = DDB.Graphiques(graphiques)



boutonstitres.initialisation()
graphiques.initialisation()


if __name__ == '__main__':
    fdb.app.run (debug = False, use_reloader=False)