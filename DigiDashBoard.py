# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 21:25:48 2020

Complète la page Flaskdigidashboard.py, c'est ici que les classes sont créées,
c'est le wrapper du module DigiDashBoard.

L'utilisateur n'aura qu'à les appeler et y mettre ses données et sa
personalisation pour avoir son dashboard.

@author: Nicolas, Reda
"""

import Flaskdigidashboard as fdb


# CLasse pour les boutons et sous-titres du tableau de bord
class Boutonstitres():
    
    boutonstitres = {}
    
    def __init__(self, boutonstitres):
        self.boutonstitres = boutonstitres
        
    def initialisation(self):
        fdb.boutonstitres = self.boutonstitres
        

        
class Graphiques():
    
    graphiques = {}
    
    def __init__(self, graphiques):
        self.graphiques = graphiques
        
    def initialisation(self):
        fdb.graphiques = self.graphiques
        

if __name__ == '__main__':
    fdb.app.run (debug = False, use_reloader=False)